package com.devindonesia.dokterproject.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.devindonesia.dokterproject.view.fragment.AddAsistent;
import com.devindonesia.dokterproject.view.fragment.ListKelolaAsisstent;

/**
 * Created by root on 23/08/17.
 */

public class KelolaAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public KelolaAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ListKelolaAsisstent tab1 = new ListKelolaAsisstent();
                return tab1;
            case 1:
                AddAsistent tab2 = new AddAsistent();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

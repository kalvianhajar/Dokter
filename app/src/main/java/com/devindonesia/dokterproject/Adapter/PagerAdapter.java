package com.devindonesia.dokterproject.Adapter;

import android.support.v4.app.FragmentManager;

/**
 * Created by root on 23/08/17.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.devindonesia.dokterproject.view.fragment.JadwalBerhalanagan;
import com.devindonesia.dokterproject.view.fragment.ListJadwalDokter;
import com.devindonesia.dokterproject.view.fragment.ListKelolaJadwal;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                //ListKelolaJadwal tab1 = new ListKelolaJadwal();
                ListJadwalDokter tab1=new ListJadwalDokter();
                return tab1;
            case 1:
                JadwalBerhalanagan tab2 = new JadwalBerhalanagan();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}
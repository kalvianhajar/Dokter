package com.devindonesia.dokterproject.Helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by root on 14/09/17.
 */

public class SharedPref {
    final  SharedPreferences preferences;
    private static final String MYPREF="antrian";
    private static final String KEY_USERNAME="username";
    private static final String KEY_LEVEL="level";
    private static final String KEY_ID="id";
    private static final String KEY_LOGIN="login";
    private static final String KEY_ID_USER="iduser";

    public SharedPref(Context context){
        preferences=context.getSharedPreferences(MYPREF,Context.MODE_PRIVATE);
    }



    public String getAccountName() {
        return preferences.getString(KEY_USERNAME, null);
    }


    public void setUsername(String accountName) {
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_USERNAME, accountName);
        editor.commit();
    }

    public void setIdUser(String id) {
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_ID_USER, id);
        editor.commit();
    }

    public String getIdUser() {
        return preferences.getString(KEY_ID_USER, null);
    }


    public String getLevelAccount() {
        return preferences.getString(KEY_LEVEL, null);
    }


    public void setLevel(String level) {
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_LEVEL, level);
        editor.commit();
    }

    public void setId(String id){
        final SharedPreferences.Editor editor=preferences.edit();
        editor.putString(KEY_ID,id);
        editor.commit();
    }

    public String getId(){
        return preferences.getString(KEY_ID,"");
    }

    public boolean getLoginStatus(){
        return preferences.getBoolean(KEY_LOGIN,false);
    }

    public void setLogin(boolean status){
        final SharedPreferences.Editor editor=preferences.edit();
        editor.putBoolean(KEY_LOGIN,status);
        editor.commit();

    }


    public  void logout(){
        final SharedPreferences.Editor editor=preferences.edit();
        editor.clear();
        editor.commit();
    }

}

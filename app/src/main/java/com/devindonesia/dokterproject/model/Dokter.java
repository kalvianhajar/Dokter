package com.devindonesia.dokterproject.model;

/**
 * Created by root on 22/08/17.
 */
public class Dokter {
    private String id;
    private  String nama;
    private String spesiali;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getSpesiali() {
        return spesiali;
    }

    public void setSpesiali(String spesiali) {
        this.spesiali = spesiali;
    }

    public Dokter(String id, String nama, String spesiali) {
        this.id = id;
        this.nama = nama;
        this.spesiali = spesiali;
    }
}

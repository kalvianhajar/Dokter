package com.devindonesia.dokterproject.model;

/**
 * Created by root on 30/09/17.
 */

public class JadwalPraktik {
    private String jadwalid;
    private String dokterid;
    private String praktikid;
    private String tgl_praktik;
    private String jammulai;
    private String jamselesia;
    private String jam1a, jam1b;
    private String jam2a, jam2b;
    private String haripraktik;

    public JadwalPraktik(String jadwalid, String dokterid, String praktikid, String tgl_praktik, String jammulai, String jamselesia, String haripraktik) {
        this.jadwalid = jadwalid;
        this.dokterid = dokterid;
        this.praktikid = praktikid;
        this.tgl_praktik = tgl_praktik;
        this.jammulai = jammulai;
        this.jamselesia = jamselesia;
        this.haripraktik = haripraktik;
    }

    public String getHaripraktik() {
        return haripraktik;
    }

    public void setHaripraktik(String haripraktik) {
        this.haripraktik = haripraktik;
    }

    public String getJadwalid() {
        return jadwalid;
    }

    public void setJadwalid(String jadwalid) {
        this.jadwalid = jadwalid;
    }

    public String getDokterid() {
        return dokterid;
    }

    public void setDokterid(String dokterid) {
        this.dokterid = dokterid;
    }

    public String getPraktikid() {
        return praktikid;
    }

    public void setPraktikid(String praktikid) {
        this.praktikid = praktikid;
    }

    public String getTgl_praktik() {
        return tgl_praktik;
    }

    public void setTgl_praktik(String tgl_praktik) {
        this.tgl_praktik = tgl_praktik;
    }

    public String getJammulai() {
        return jammulai;
    }

    public void setJammulai(String jammulai) {
        this.jammulai = jammulai;
    }

    public String getJamselesia() {
        return jamselesia;
    }

    public void setJamselesia(String jamselesia) {
        this.jamselesia = jamselesia;
    }

    public String getJam1a() {
        return jam1a;
    }

    public void setJam1a(String jam1a) {
        this.jam1a = jam1a;
    }

    public String getJam1b() {
        return jam1b;
    }

    public void setJam1b(String jam1b) {
        this.jam1b = jam1b;
    }

    public String getJam2a() {
        return jam2a;
    }

    public void setJam2a(String jam2a) {
        this.jam2a = jam2a;
    }

    public String getJam2b() {
        return jam2b;
    }

    public void setJam2b(String jam2b) {
        this.jam2b = jam2b;
    }
}

package com.devindonesia.dokterproject.model;

/**
 * Created by root on 23/08/17.
 */
public class ListJadwal {

    private String id;
    private String status;
    private String jam;
    private String idpasien;
    private String namapasien;
    private String iddokter;
    private String idpraktik;
    private String periksa;


    public ListJadwal(String id, String status,String  jam) {
        this.id = id;
        this.status = status;
        this.jam=jam;
    }

    public ListJadwal(String id, String status, String jam, String idpasien, String namapasien, String iddokter, String idpraktik) {
        this.id = id;
        this.status = status;
        this.jam = jam;
        this.idpasien = idpasien;
        this.namapasien = namapasien;
        this.iddokter = iddokter;
        this.idpraktik = idpraktik;
    }

    public ListJadwal(String id, String status, String jam, String idpasien, String namapasien) {
        this.id = id;
        this.status = status;
        this.jam = jam;
        this.idpasien = idpasien;
        this.namapasien = namapasien;
    }

    public ListJadwal(String id, String status, String jam, String idpasien, String namapasien,String periksa) {
        this.id = id;
        this.status = status;
        this.jam = jam;
        this.idpasien = idpasien;
        this.namapasien = namapasien;
        this.periksa=periksa;
    }


    public String getIddokter() {
        return iddokter;
    }

    public void setIddokter(String iddokter) {
        this.iddokter = iddokter;
    }

    public String getIdpraktik() {
        return idpraktik;
    }

    public void setIdpraktik(String idpraktik) {
        this.idpraktik = idpraktik;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getIdpasien() {
        return idpasien;
    }

    public void setIdpasien(String idpasien) {
        this.idpasien = idpasien;
    }

    public String getNamapasien() {
        return namapasien;
    }

    public void setNamapasien(String namapasien) {
        this.namapasien = namapasien;
    }

    public String getPeriksa() {
        return periksa;
    }

    public void setPeriksa(String periksa) {
        this.periksa = periksa;
    }
}




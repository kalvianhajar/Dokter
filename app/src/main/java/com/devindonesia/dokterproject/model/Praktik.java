package com.devindonesia.dokterproject.model;

/**
 * Created by root on 04/09/17.
 */

public class Praktik {
    private String id;
    private String nama;
    private String ket;

    public Praktik(String id, String nama, String ket) {
        this.id = id;
        this.nama = nama;
        this.ket = ket;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }



}

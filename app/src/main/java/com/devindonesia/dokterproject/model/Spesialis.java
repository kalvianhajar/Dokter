package com.devindonesia.dokterproject.model;

/**
 * Created by root on 04/09/17.
 */

public class Spesialis {
    private String id;
    private String nama;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Spesialis(String id, String nama) {
        this.id = id;
        this.nama = nama;
    }
}

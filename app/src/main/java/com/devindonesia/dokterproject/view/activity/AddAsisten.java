package com.devindonesia.dokterproject.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.Praktik;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddAsisten extends AppCompatActivity implements View.OnClickListener {
    Spinner spPraktik;
    private ArrayList<Praktik> data = new ArrayList<>();
    ArrayList<String> dataPraktik = new ArrayList<>();
    ArrayAdapter<String> adapter;
    private String JSON_URL = "http://dokter.themastah.com/api/praktik";
    private Button btnKembali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_asisten);
        spPraktik=(Spinner)findViewById(R.id.spPraktik);
        adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_row, R.id.tvSpinnner, dataPraktik);
        spPraktik.setAdapter(adapter);
        btnKembali=(Button)findViewById(R.id.btnKembali);
        btnKembali.setOnClickListener(this);
        sendRequest();
    }

    private void sendRequest() {
        StringRequest stringRequest = new StringRequest(JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("data"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String nama = obj.getString("praktik_nama");
                                String id = obj.getString("praktik_id");
                                String ket = obj.getString("praktik_ket");
                                Praktik praktik = new Praktik(id, nama, ket);
                                data.add(praktik);
                                dataPraktik.add(nama);

                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        if(view==btnKembali){
            finish();
        }
    }
}

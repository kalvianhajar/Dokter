package com.devindonesia.dokterproject.view.activity;

/**
 * Created by root on 14/09/17.
 */
public class Asistent {
    private String id;
    private String namaasisten;
    private String idpraktik;
    private String namapraktik;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaasisten() {
        return namaasisten;
    }

    public void setNamaasisten(String namaasisten) {
        this.namaasisten = namaasisten;
    }

    public String getIdpraktik() {
        return idpraktik;
    }

    public void setIdpraktik(String idpraktik) {
        this.idpraktik = idpraktik;
    }

    public String getNamapraktik() {
        return namapraktik;
    }

    public Asistent(String id, String namaasisten, String idpraktik, String namapraktik) {
        this.id = id;
        this.namaasisten = namaasisten;
        this.idpraktik = idpraktik;
        this.namapraktik = namapraktik;
    }

    public void setNamapraktik(String namapraktik) {
        this.namapraktik = namapraktik;

    }
}

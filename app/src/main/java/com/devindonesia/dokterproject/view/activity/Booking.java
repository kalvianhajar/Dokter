package com.devindonesia.dokterproject.view.activity;

/**
 * Created by root on 15/09/17.
 */
public class Booking {
    private String id;
    private String nobooking;
    private String alamat;
    private String status;

    public Booking(String id, String nobooking, String waktu,String alamat,String status) {
        this.id = id;
        this.nobooking = nobooking;
        this.waktu = waktu;
        this.alamat=alamat;
        this.status=status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNobooking() {
        return nobooking;
    }

    public void setNobooking(String nobooking) {
        this.nobooking = nobooking;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    private String waktu;

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.devindonesia.dokterproject.view.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.Praktik;
import com.devindonesia.dokterproject.model.Spesialis;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FormRegisterDokterActivity extends AppCompatActivity implements View.OnClickListener {
    private Spinner spPraktik, spSpesialis;
    private ArrayList<Praktik> data = new ArrayList<>();
    private ArrayList<Spesialis> data1 = new ArrayList<>();
    private String JSON_URL = "http://dokter.themastah.com/api/praktik";
    private String JSON_URL1 = "http://dokter.themastah.com/api/spesialis";
    private String JSON_URL2 = "http://dokter.themastah.com/api/dokter";
    ArrayAdapter<String> adapter, adapter1;
    ArrayList<String> dataPraktik = new ArrayList<>();
    ArrayList<String> dataSpesialis = new ArrayList<>();
    private Button btnRegisterDokter;
    private EditText tvNamaDokter, tvHpDokter, tvEmailDokter, tvUsername, tvPassword, tvKonfirmasiPassword;
    private String KEY_NAMA = "dokter_nama", KEY_HP = "dokter_hp", KEY_EMAIL = "dokter_email", KEY_USERNAME = "dokter_username", KEY_PASSWORD = "dokter_password";
    private String KEY_SPESIALIS = "spesialis_id";
    private String spesialis,praktik;
    private String image = "", estimasi = "1", alamat = "Indonesia";
    private String KEY_PRaKTIK = "praktik_id";
    private String KEY_IMAGE = "image_id";
    private String KEY_ESTIMASi = "dokter_estimasi";
    private String KEY_ALAMAT = "dokter_alamat";
    private String TAG = FormRegisterDokterActivity.class.getSimpleName();
    private String KEY_STATUS="status";
    private String KEY_DOKTER="dokter";

    private void setupView() {
        tvNamaDokter = (EditText) findViewById(R.id.tvNamaDokter);
        tvHpDokter = (EditText) findViewById(R.id.tvHpDokter);
        tvEmailDokter = (EditText) findViewById(R.id.tvEmailDokter);
        tvUsername = (EditText) findViewById(R.id.tvUsername);
        tvPassword = (EditText) findViewById(R.id.tvPassword);
        tvKonfirmasiPassword = (EditText) findViewById(R.id.tvKonfirmasiPassword);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_register);
        spPraktik = (Spinner) findViewById(R.id.spPraktik);
        spSpesialis = (Spinner) findViewById(R.id.spSpesialis);
        btnRegisterDokter = (Button) findViewById(R.id.btnRegisterDokter);
        btnRegisterDokter.setOnClickListener(this);
        adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_row, R.id.tvSpinnner, dataPraktik);
        adapter1 = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_row, R.id.tvSpinnner, dataSpesialis);
        spPraktik.setAdapter(adapter);
        spSpesialis.setAdapter(adapter1);
        spPraktik.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                praktik =data.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spSpesialis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spesialis =data1.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        setupView();
        sendRequest1();
        sendRequest();

    }

    private void sendRequest() {
        StringRequest stringRequest = new StringRequest(JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("data"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String nama = obj.getString("praktik_nama");
                                String id = obj.getString("praktik_id");
                                String ket = obj.getString("praktik_ket");
                                Praktik praktik = new Praktik(id, nama, ket);
                                data.add(praktik);
                                dataPraktik.add(nama);

                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void sendRequest1() {
        StringRequest stringRequest = new StringRequest(JSON_URL1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("data"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String nama = obj.getString("spesialis_nama");
                                String id = obj.getString("spesialis_id");
                                Spesialis spesialis = new Spesialis(id, nama);
                                data1.add(spesialis);
                                dataSpesialis.add(nama);

                            }
                            adapter1.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    @Override
    public void onClick(View view) {
        if (view == btnRegisterDokter) {
            final ProgressDialog pDialog = new ProgressDialog(this);
            pDialog.setMessage("Loading...");
            pDialog.show();

            final String nama = tvNamaDokter.getText().toString();
            final String hp = tvHpDokter.getText().toString();
            final String email = tvEmailDokter.getText().toString();
            final String username = tvUsername.getText().toString();
            final String password = tvPassword.getText().toString();
            String konfirmasi = tvKonfirmasiPassword.getText().toString();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, JSON_URL2,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pDialog.dismiss();
                            Log.e(TAG,response);
                            clearForm();
                            Toast.makeText(getApplicationContext(),"Register Dokter Sukses",Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pDialog.dismiss();
                            Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {


                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(KEY_NAMA, nama);
                    params.put(KEY_SPESIALIS, spesialis);
                    params.put(KEY_PRaKTIK, String.valueOf(praktik));
                    params.put(KEY_IMAGE, image);
                    params.put(KEY_HP, hp);
                    params.put(KEY_EMAIL, email);
                    params.put(KEY_ALAMAT,alamat);
                    params.put(KEY_ESTIMASi,estimasi);
                    params.put(KEY_USERNAME, username);
                    params.put(KEY_PASSWORD, password);
                    params.put(KEY_STATUS,KEY_DOKTER);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
        }
    }

    private void clearForm(){
       tvNamaDokter.setText("");
        tvHpDokter.setText("");
         tvEmailDokter.setText("");
         tvUsername.setText("");
        tvPassword.setText("");
         tvKonfirmasiPassword.setText("");
    }

}



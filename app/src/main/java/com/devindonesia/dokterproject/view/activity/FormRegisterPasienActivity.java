package com.devindonesia.dokterproject.view.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.devindonesia.dokterproject.Helper.AppController;
import com.devindonesia.dokterproject.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FormRegisterPasienActivity extends AppCompatActivity implements View.OnClickListener {
    private String[] data={"Laki-laki","Wanita"};
    private String[] data1={"Ansuransi","Umum"};
    private Spinner spJK,spAnsuransi;
    private EditText tvNamaLengkap,tvPekerjaan,tvHandphone,tvEmail,tvAlamat,tvUsername,tvPassword,tvKonfirmasiPassword,tvAnsuransi;
    private Button btnDaftar;
    private ProgressDialog  progressDialog;
    private String URLRegister="http://dokter.themastah.com/api/pasien";
    private String TAG=FormRegisterPasienActivity.class.getSimpleName();
    private String JK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_register_pasien);
       setupView();
    }

    private void showDialog(){
        progressDialog=new ProgressDialog(FormRegisterPasienActivity.this);
        progressDialog.setTitle("Register User");
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    private void hideDialog(){
        progressDialog.dismiss();
    }

    private void setupView(){
        spJK=(Spinner)findViewById(R.id.spJK);
        spAnsuransi=(Spinner)findViewById(R.id.spAnsuransi);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getApplicationContext(),R.layout.simple_row,R.id.tvSpinnner,data);
        ArrayAdapter<String> adapter1=new ArrayAdapter<String>(getApplicationContext(),R.layout.simple_row,R.id.tvSpinnner,data1);
        spJK.setAdapter(adapter);
        spJK.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(spJK.getSelectedItemPosition()==1){
                    JK="W";
                }else{
                    JK="P";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spAnsuransi.setAdapter(adapter1);
        btnDaftar=(Button)findViewById(R.id.btnDaftar);
        tvNamaLengkap=(EditText)findViewById(R.id.tvNamaLengkap);
        tvPekerjaan=(EditText)findViewById(R.id.tvPekerjaan);
        tvHandphone=(EditText)findViewById(R.id.tvHandphone);
        tvEmail=(EditText)findViewById(R.id.tvEmail);
        tvAlamat=(EditText)findViewById(R.id.tvAlamat);
        tvUsername=(EditText)findViewById(R.id.tvUsername);
        tvPassword=(EditText)findViewById(R.id.tvPassword);
        tvKonfirmasiPassword=(EditText)findViewById(R.id.tvKonfirmasiPassword);
        tvAnsuransi=(EditText)findViewById(R.id.tvAnsuransi);
        btnDaftar.setOnClickListener(this);
    }

    private void clearform(){
        tvNamaLengkap.setText("");
        tvPekerjaan.setText("");
        tvHandphone.setText("");
        tvEmail.setText("");
        tvAlamat.setText("");
        tvUsername.setText("");
        tvPassword.setText("");
        tvKonfirmasiPassword.setText("");
    }

    @Override
    public void onClick(View view) {
        if(view==btnDaftar){
            showDialog();
                final String nama=tvNamaLengkap.getText().toString();
                final String pekerjaan=tvPekerjaan.getText().toString();
                final String handphone=tvHandphone.getText().toString();
                final String email=tvEmail.getText().toString();
                final String username=tvUsername.getText().toString();
                final String password=tvPassword.getText().toString();
                final String konfirmasi=tvKonfirmasiPassword.getText().toString();
                final String ansunsi="A";
                final String alamat=tvAlamat.getText().toString();


            StringRequest request=new StringRequest(Request.Method.POST, URLRegister, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    hideDialog();

                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        String status=jsonObject.getString("status");

                        if(status.equalsIgnoreCase("true")){
                            clearform();
                            Toast.makeText(getApplicationContext(),"Registrasi Akun pasien berhasil, Silahkan Ikuti Instruksi selanjutnya.",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getApplicationContext(),"Gagal Mendaftar",Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideDialog();
                          Log.e(TAG,error.toString());
                }
            }){
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username",username);
                    params.put("password",password);
                    params.put("pasien_nama",nama);
                    params.put("pasien_jk",JK);
                    params.put("pasien_pekerjaan",pekerjaan);
                    params.put("pasien_nohp",handphone);
                    params.put("pasien_email",email);
                    params.put("pasien_alamat",alamat);
                    params.put("pasien_ansuransi",ansunsi);
                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue(request,TAG);


        }
    }
}

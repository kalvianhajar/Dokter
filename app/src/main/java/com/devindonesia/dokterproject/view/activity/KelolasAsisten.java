package com.devindonesia.dokterproject.view.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.Dokter;
import com.devindonesia.dokterproject.view.fragment.ListDokterActivity;
import com.devindonesia.dokterproject.view.fragment.ListDokterHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.devindonesia.dokterproject.R.id.rvDokter;

public class KelolasAsisten extends AppCompatActivity implements View.OnClickListener {
    RecyclerView rvAsistent;
    private ProgressDialog progressDialog;
    private AdapterRecylerviewAll<Asistent, ListAsistentHolder> adapter;
    private ArrayList<Asistent> asistentArrayList = new ArrayList<>();
    private String JSON_URL = "http://dokter.themastah.com/api/asisten";
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kelolas_asisten);
        rvAsistent = (RecyclerView) findViewById(R.id.rvAsistent);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        progressDialog = new ProgressDialog(KelolasAsisten.this);
        progressDialog.setTitle("Asisten");
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        rvAsistent.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new AdapterRecylerviewAll<Asistent, ListAsistentHolder>(R.layout.row_asistent, ListAsistentHolder.class, Asistent.class, asistentArrayList) {
            @Override
            protected void bindView(ListAsistentHolder holder, final Asistent dokter, int position) {
                holder.initData(dokter);
            }
        };
        rvAsistent.setAdapter(adapter);
        fab.setOnClickListener(this);
        sendRequest();
    }

    private void sendRequest() {
        StringRequest stringRequest = new StringRequest(JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("data"));


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String nama = obj.getString("asisten_nama");
                                String id = obj.getString("asisten_id");
                                String praktik = obj.getString("praktik_id");
                                String praktik_nama = obj.getString("praktik_nama");
                                Asistent asistent = new Asistent(id, nama, praktik, praktik_nama);
                                asistentArrayList.add(asistent);
                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        if (view == fab) {
            startActivity(new Intent(getApplicationContext(), AddAsisten.class));
        }
    }
}

package com.devindonesia.dokterproject.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.JadwalPraktik;
import com.devindonesia.dokterproject.model.ListJadwal;
import com.devindonesia.dokterproject.view.fragment.ListAntreanHolder;
import com.devindonesia.dokterproject.view.fragment.ListDokterActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ListAntrianDokter extends AppCompatActivity {
    private RecyclerView rvListAntrean;
    private ArrayList<ListJadwal> mData = new ArrayList<>();
    private AdapterRecylerviewAll<ListJadwal, ListAntreanHolder> adapter;
    private String JSON_TGL = "http://dokter.themastah.com/api/jadwal/antriantgl";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_antrian_dokter);
        rvListAntrean=(RecyclerView)findViewById(R.id.rvListAntrean);
        adapter = new AdapterRecylerviewAll<ListJadwal, ListAntreanHolder>(R.layout.row_listjadwal, ListAntreanHolder.class, ListJadwal.class, mData) {
            @Override
            protected void bindView(final ListAntreanHolder holder, final ListJadwal model, final int position) {
                holder.setData(model);
                holder.container_booking.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.setData(model);

                    }

                });
            }
        };

        rvListAntrean.setAdapter(adapter);
       // getJadwal();
    }

    private void getJadwal(final String dokterid, final String tgl, final String praktik) {
        mData.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, JSON_TGL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {


                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject obj = jsonArray.getJSONObject(i);
                                String jammulai = obj.getString("jam_praktik").substring(0, 5);

                                String status = obj.getString("status");
                                String pasien = obj.getString("pasien_nama");
                                String jadwalid = obj.getString("jadwal_detail_id");

                                mData.add(new ListJadwal("" + (i + 1), status, jammulai, jadwalid, pasien));

                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.toString();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dokter_id", dokterid);
                params.put("tgl", tgl);
                params.put("praktik_id", praktik);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }
}

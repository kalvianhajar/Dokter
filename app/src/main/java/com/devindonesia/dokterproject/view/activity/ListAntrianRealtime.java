package com.devindonesia.dokterproject.view.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.ListJadwal;
import com.devindonesia.dokterproject.view.fragment.ListAntreanHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class ListAntrianRealtime extends AppCompatActivity {
    private RecyclerView rvListAntrean;
    private ArrayList<ListJadwal> mData = new ArrayList<>();
    private AdapterRecylerviewAll<ListJadwal, ListAntreanHolder> adapter;
    String jammulai;
    private String iddokter, idpraktik;
    private String tgl;
    private TextView tvAntrian, tvTgl;
    private String JSON_JADWAL = "http://dokter.themastah.com/api/jadwal/datajadwal";
    private String JSON_ANTRIAN = "http://dokter.themastah.com/api/jadwal/getuseraktiv";
    private String[] namabulan = {"Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "Sepetember", "Oktober", "November", "Desember"};
    Timer timer;
    TimerTask timerTask;
    final Handler handler = new Handler();
    protected ProgressBar progress;
    private String[] hari={"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu"};
    private String hariini;


    @Override
    protected void onResume() {
        super.onResume();
        startTimer();
    }

    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 5000, 10000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stoptimertask();
    }

    public void stoptimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            getJadwal(iddokter, tgl, idpraktik);
                            getAntrian(iddokter, tgl);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }

        };

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_antrian_realtime);
        rvListAntrean = (RecyclerView) findViewById(R.id.rvListAntrean);
        progress = (ProgressBar) findViewById(R.id.progress);
        progress.setVisibility(View.GONE);
        tvAntrian = (TextView) findViewById(R.id.tvAntrian);
        tvTgl = (TextView) findViewById(R.id.tvTgl);
        rvListAntrean.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new AdapterRecylerviewAll<ListJadwal, ListAntreanHolder>(R.layout.row_listjadwal, ListAntreanHolder.class, ListJadwal.class, mData) {
            @Override
            protected void bindView(ListAntreanHolder holder, ListJadwal model, int position) {
                holder.setData(model);
                if (model.getPeriksa().equalsIgnoreCase("ya")) {
                    if(model.getStatus().equalsIgnoreCase("Done")){
                        holder.tvDone.setVisibility(View.VISIBLE);
                        holder.container_booking.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
                    }else {
                        holder.container_booking.setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                        holder.tvDone.setVisibility(View.GONE);
                    }
                } else {
                    holder.tvDone.setVisibility(View.GONE);
                    holder.container_booking.setBackgroundColor(getResources().getColor(android.R.color.white));
                }
            }
        };
        rvListAntrean.setAdapter(adapter);
        Intent intent = getIntent();
        idpraktik = intent.getStringExtra("idpraktik");
        iddokter = intent.getStringExtra("iddokter");

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        int hari1=calendar.get(Calendar.DAY_OF_WEEK);
        String day1, month1;
        hariini="Selasa";

        if (String.valueOf(day).length() < 2) {
            day1 = "0" + day;
        } else {
            day1 = String.valueOf(day);
        }
        if (String.valueOf(month).length() < 2) {
            month1 = "0" + month;
        } else {
            month1 = String.valueOf(month);
        }

        tgl = year + "-" + month1 + "-" + day1;
        getJadwal(idpraktik, tgl, iddokter);
        getAntrian(iddokter, tgl);
    }


    private void getJadwal(final String dokter, final String tgl, final String praktik) {
        mData.clear();
        progress.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, JSON_JADWAL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            progress.setVisibility(View.GONE);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject obj = jsonArray.getJSONObject(i);
                                jammulai = obj.getString("jam_praktik").substring(0, 5);
                                String status = obj.getString("status");
                                String pasien = obj.getString("pasien_nama");
                                String jadwalid = obj.getString("jadwal_detail_id");
                                String periksa = obj.getString("periksa");

                                mData.add(new ListJadwal("" + (i + 1), status, jammulai, jadwalid, pasien, periksa));

                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dokter_id",dokter);
                params.put("tgl", tgl);
                params.put("praktik_id", praktik);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getAntrian(final String dokter, final String tgl) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, JSON_ANTRIAN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                final String result = obj.getString("jadwal_detail_id");
                                final String tgl = obj.getString("tgl_booking");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String tahun = tgl.substring(0, 4);
                                        String bulan = tgl.substring(6, 7);
                                        String tgll = tgl.substring(8, 10);
                                        tvAntrian.setText(result);
                                        tvTgl.setText(hariini+","+tgll + "-" + namabulan[Integer.parseInt(bulan)] + "-" + tahun);

                                    }
                                });


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dokter_id",dokter);
                params.put("tgl", tgl);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}

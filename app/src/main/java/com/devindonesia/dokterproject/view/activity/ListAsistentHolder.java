package com.devindonesia.dokterproject.view.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.devindonesia.dokterproject.R;

/**
 * Created by root on 14/09/17.
 */
public class ListAsistentHolder extends RecyclerView.ViewHolder {

    public TextView tvNamaAistent,tvPraktik;
    public ListAsistentHolder(View itemView) {
        super(itemView);
        tvNamaAistent=(TextView)itemView.findViewById(R.id.tvNamaAistent);
        tvPraktik=(TextView)itemView.findViewById(R.id.tvPraktik);
    }

    public void initData(Asistent asistent){
        tvNamaAistent.setText(asistent.getNamaasisten());
        tvPraktik.setText(asistent.getNamapraktik());
    }
}

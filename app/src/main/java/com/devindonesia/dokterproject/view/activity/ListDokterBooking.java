package com.devindonesia.dokterproject.view.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.Dokter;
import com.devindonesia.dokterproject.view.fragment.ListDokter;
import com.devindonesia.dokterproject.view.fragment.ListDokterActivity;
import com.devindonesia.dokterproject.view.fragment.ListDokterHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListDokterBooking extends AppCompatActivity {
    private RecyclerView rvDokter;
    private AdapterRecylerviewAll<Dokter,ListDokterHolder> adapter;
    private ArrayList<Dokter> dokterArrayList=new ArrayList<>();
    private String JSON_URL="http://dokter.themastah.com/api/dokter";
    private String TAG=ListDokter.class.getSimpleName();
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_dokter_booking);
        rvDokter = (RecyclerView)findViewById(R.id.rvDokter);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(ListDokterBooking.this);
        progressDialog.setTitle("Dokter");
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        rvDokter.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter =new AdapterRecylerviewAll<Dokter, ListDokterHolder>(R.layout.row_dokter,ListDokterHolder.class,Dokter.class,dokterArrayList) {
            @Override
            protected void bindView(ListDokterHolder holder, final Dokter dokter, int position) {
                holder.initData(dokter);
                holder.container_dokter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
            }
        };
        rvDokter.setAdapter(adapter);
        sendRequest();
    }

    private void sendRequest(){
        StringRequest stringRequest = new StringRequest(JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response.toString());
                            JSONArray jsonArray=new JSONArray(jsonObject.getString("data"));
                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject obj=jsonArray.getJSONObject(i);
                                String nama=obj.getString("dokter_nama");
                                String id=obj.getString("dokter_id");
                                String spesialis=obj.getString("spesialis_nama");
                                Dokter dokter=new Dokter(id,nama,spesialis);
                                dokterArrayList.add(dokter);
                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }
}

package com.devindonesia.dokterproject.view.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.Dokter;
import com.devindonesia.dokterproject.view.fragment.JadwalDokter;
import com.devindonesia.dokterproject.view.fragment.ListDokterActivity;
import com.devindonesia.dokterproject.view.fragment.ListDokterHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListHistory extends AppCompatActivity {
    private RecyclerView rvHistoryBooking;
    private AdapterRecylerviewAll<Booking,ListDokterHolder> adapter;
    private ArrayList<Booking> dokterArrayList=new ArrayList<>();
    private String URL_HISTORY="http://dokter.themastah.com/api/booking";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_history);
        rvHistoryBooking=(RecyclerView)findViewById(R.id.rvHistoryBooking);
        rvHistoryBooking.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter =new AdapterRecylerviewAll<Booking, ListDokterHolder>(R.layout.row_booking,ListDokterHolder.class,Booking.class,dokterArrayList) {
            @Override
            protected void bindView(ListDokterHolder holder, final Booking dokter, int position) {
                holder.setBooking(dokter);
                holder.container_dokter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
            }
        };
        rvHistoryBooking.setAdapter(adapter);
        setData();
    }
    private void  setData(){
        final ProgressDialog pDialog = new ProgressDialog(ListHistory.this);
        pDialog.setMessage("Loading...");
        pDialog.setTitle("History");
        pDialog.show();

        StringRequest stringRequest = new StringRequest(URL_HISTORY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            JSONArray jsonArray=jsonObject.getJSONArray("data");
                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject obj=jsonArray.getJSONObject(i);

                                String booking_id=obj.getString("booking_id");
                                String booking_tgl=obj.getString("tgl_booking");
                                String dokter_nama=obj.getString("dokter_nama");
                                String praktik_nama=obj.getString("praktik_nama");
                                String status=obj.getString("status");

                                Booking jadwalDokter=new Booking(booking_id,booking_tgl,dokter_nama,praktik_nama,status);
                                dokterArrayList.add(jadwalDokter);

                            }

                            //set ke adapter

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


        adapter.notifyDataSetChanged();

    }
}

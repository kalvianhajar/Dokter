package com.devindonesia.dokterproject.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.ListJadwal;
import com.devindonesia.dokterproject.view.fragment.ListAntreanHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListRumahSakit extends AppCompatActivity {
    private RecyclerView rvListRumahSakit;
    private AdapterRecylerviewAll<RumahSakit,ListRumahSakitHolder> adapter;
    private ArrayList<RumahSakit> mData=new ArrayList<>();
    private String URL="http://dokter.themastah.com/api/jadwal/getrs?id=";
    private ProgressBar progress;
    private TextView tvMsg;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mData.clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_rumah_sakit);
        rvListRumahSakit=(RecyclerView)findViewById(R.id.rvListRumahSakit);
        tvMsg=(TextView)findViewById(R.id.tvMsg);
        progress=(ProgressBar)findViewById(R.id.progress);
        rvListRumahSakit.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter=new AdapterRecylerviewAll<RumahSakit,ListRumahSakitHolder>(R.layout.row_rumah_sakit,ListRumahSakitHolder.class,RumahSakit.class,mData) {
            @Override
            protected void bindView(ListRumahSakitHolder holder, final RumahSakit model, int position) {
                holder.setData(model);
                holder.container_rs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       Intent intent=new Intent(getApplicationContext(),ListAntrianRealtime.class);
                        intent.putExtra("idpraktik",model.getId());
                        intent.putExtra("iddokter",model.getDokter_id());
                        startActivity(intent);
                    }
                });
            }
        };
        rvListRumahSakit.setAdapter(adapter);
        String id=new SharedPref(getApplicationContext()).getIdUser();
        sendRequest(id);
    }

    //get data dokter
    private void sendRequest(String id) {
        mData.clear();
        progress.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(URL+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progress.setVisibility(View.GONE);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);

                                String id=obj.getString("praktik_id");
                                String nama=obj.getString("praktik_nama");
                                String alamat=obj.getString("praktik_alamat");
                                String dokter_id=obj.getString("dokter_id");

                                mData.add(new RumahSakit(id,nama,alamat,dokter_id));
                            }

                            if(mData.size()>0){
                                tvMsg.setVisibility(View.GONE);
                                adapter.notifyDataSetChanged();
                                rvListRumahSakit.setVisibility(View.VISIBLE);
                            }else{
                                rvListRumahSakit.setVisibility(View.GONE);
                                tvMsg.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}

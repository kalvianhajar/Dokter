package com.devindonesia.dokterproject.view.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devindonesia.dokterproject.R;


/**
 * Created by root on 09/10/17.
 */
public class ListRumahSakitHolder  extends RecyclerView.ViewHolder{

    private TextView tvNama,tvAlamat;
    public LinearLayout container_rs;

    public ListRumahSakitHolder(View itemView) {
        super(itemView);
        tvNama=(TextView)itemView.findViewById(R.id.tvNama);
        tvAlamat=(TextView)itemView.findViewById(R.id.tvAlamat);
        container_rs=(LinearLayout) itemView.findViewById(R.id.container_rs);
    }
    public  void  setData(RumahSakit rumahSakit){
        tvNama.setText(rumahSakit.getNama());
        tvAlamat.setText(rumahSakit.getAlamat());
    }
}

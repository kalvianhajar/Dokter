package com.devindonesia.dokterproject.view.activity;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.view.fragment.AsistenFragment;
import com.devindonesia.dokterproject.view.fragment.DokterFragment;
import com.devindonesia.dokterproject.view.fragment.FragmentDrawer;
import com.devindonesia.dokterproject.view.fragment.HomeFragment;
import com.devindonesia.dokterproject.view.fragment.PasienFragment;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener{
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private TextView txUsername;
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        sharedPref=new SharedPref(getApplicationContext());
        String level=sharedPref.getLevelAccount();

        if(level.equalsIgnoreCase("dokter")) {
            displayView(2);
        }else if(level.equalsIgnoreCase("pasien")) {
            displayView(0);
        }else if(level.equalsIgnoreCase("asisten")){
            displayView(1);
        }else {
            displayView(0);
        }
    }


    @Override
    public void onBackPressed(){
        android.app.FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       int id = item.getItemId();
        if (id == R.id.action_logout) {
            sharedPref.logout();
            startActivity(new Intent(getApplicationContext(),UserActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position) {
            case 0:
                fragment = new PasienFragment();
                title = getString(R.string.title_home);
                break;
            case 1:
                fragment = new AsistenFragment();
                title = getString(R.string.asissten);
                break;
            case 2:
                fragment=new DokterFragment();
                title=getString(R.string.dokter);
                break;
            default:
                fragment=new HomeFragment();
                title=getString(R.string.title_home);
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(fragment.toString());
            fragmentTransaction.commit();
            getSupportActionBar().setTitle(title);
        }
    }


}


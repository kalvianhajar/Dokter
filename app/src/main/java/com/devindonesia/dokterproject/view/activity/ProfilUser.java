package com.devindonesia.dokterproject.view.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.R;

/**
 * Created by root on 14/09/17.
 */
public class ProfilUser extends Fragment implements View.OnClickListener{
    private TextView tvUsername,tvLevel;
    private SharedPref sharedPref;
    private Button btnLogout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profileuser,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPref=new SharedPref(getContext());
        tvUsername=(TextView)view.findViewById(R.id.tvUsername);
        tvLevel=(TextView)view.findViewById(R.id.tvLevel);
        btnLogout=(Button)view.findViewById(R.id.btnLogout);
        tvUsername.setText(sharedPref.getAccountName());
        tvLevel.setText(sharedPref.getLevelAccount());
        btnLogout.setOnClickListener(this);
    }

    private void logout(final Context context){
        final AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle("Logout");
        builder1.setMessage("Ingin logout ??");
        builder1.setPositiveButton(
                "Ya",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sharedPref.logout();
                        context.startActivity(new Intent(getContext(),UserActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                });
        builder1.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder1.setCancelable(true);
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onClick(View view) {
        if (view==btnLogout){
            logout(getContext());
        }
    }
}

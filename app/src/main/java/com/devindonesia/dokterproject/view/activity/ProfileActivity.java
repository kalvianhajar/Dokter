package com.devindonesia.dokterproject.view.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.Dokter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvUsername, tvLevel;
    private SharedPref sharedPref;
    private Button btnLogout;
    private String JSON_URL = "http://dokter.themastah.com/api/pasien";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        sharedPref = new SharedPref(getApplicationContext());
        tvUsername = (TextView) findViewById(R.id.tvUsername);
        tvLevel = (TextView) findViewById(R.id.tvLevel);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        tvUsername.setText(sharedPref.getAccountName());
        tvLevel.setText(sharedPref.getLevelAccount());
        sendRequest("1");
    }

    private void sendRequest(String id) {
        StringRequest stringRequest = new StringRequest(JSON_URL + "?id=" + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("data"));
                            Log.e("LOG", jsonArray.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    @Override
    public void onClick(View view) {

    }
}

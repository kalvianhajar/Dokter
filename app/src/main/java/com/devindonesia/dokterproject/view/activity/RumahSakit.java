package com.devindonesia.dokterproject.view.activity;

/**
 * Created by root on 09/10/17.
 */
public class RumahSakit {
    public RumahSakit(String id, String nama, String alamat) {
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
    }

    public RumahSakit(String id, String nama, String alamat, String dokter_id) {
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
        this.dokter_id = dokter_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDokter_id() {
        return dokter_id;
    }

    public void setDokter_id(String dokter_id) {
        this.dokter_id = dokter_id;
    }

    private String id;
    private String nama;
    private String alamat;
    private String dokter_id;




}

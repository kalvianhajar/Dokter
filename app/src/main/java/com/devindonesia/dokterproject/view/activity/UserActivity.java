package com.devindonesia.dokterproject.view.activity;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.view.fragment.ListDokterActivity;
import com.devindonesia.dokterproject.view.fragment.LoginFragment;
import com.devindonesia.dokterproject.view.fragment.RegisterFragment;

public class UserActivity extends AppCompatActivity implements View.OnClickListener {
    private Button  btnCari;
    LinearLayout linearLayout;
    private String TAG = UserActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        btnCari = (Button) findViewById(R.id.btnCari);
        btnCari.setOnClickListener(this);
        linearLayout = (LinearLayout) findViewById(R.id.container_user);
        addLogin();
    }


    @Override
    public void onBackPressed(){
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }

    public void addLogin() {
        getSupportFragmentManager().beginTransaction().add(R.id.container_user, new LoginFragment(), TAG).addToBackStack("login").commit();
    }


    public void replaceLogin() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container_user, new LoginFragment(), TAG).addToBackStack("loginreplae").commit();
    }

    public void addRegister() {
        btnCari.setVisibility(View.GONE);
        getSupportFragmentManager().beginTransaction().replace(R.id.container_user, new RegisterFragment(), TAG).addToBackStack("register").commit();
    }


    @Override
    public void onClick(View view) {
        if(view==btnCari){
            startActivity(new Intent(getApplicationContext(),ListDokterActivity.class));
        }
    }
}

package com.devindonesia.dokterproject.view.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 20/09/17.
 */
public class DetailBooking extends Fragment {
    private Button btnBooking;
    private String[] data1 = {"Ansuransi", "Umum"};
    private Spinner spAnsuransi;
    private EditText etAntrian;
    private CheckBox ckdiri;
    private EditText etNama, etUmur, etKK, etAlamat;
    private String JSON_BOOKING = "http://dokter.themastah.com/api/jadwal/updatejadwalbymember";
    private String JSON_BOOKING1 = "http://dokter.themastah.com/api/jadwal/updateuserbook";
    private String JSON_JADWAL = "http://dokter.themastah.com/api/jadwal";
    private String ANSURANSI = "A";
    private boolean statusupload = false;
    private String idjadwal,refuser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((ListDokterActivity) getActivity()).toolbar.setTitle("Detail Booking");
        return inflater.inflate(R.layout.detialbooking, container, false);
    }



    private void uploadUser() {
        final ProgressDialog pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.setTitle("Booking");
        pDialog.show();


        
  StringRequest stringRequest = new StringRequest(Request.Method.POST, JSON_BOOKING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        pDialog.dismiss();

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.getString("status");

                                    if (status.equalsIgnoreCase("true")) {
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                        builder1.setTitle("Booking Sukses");
                                        builder1.setMessage("Booking Telah Berhasil di Buat.");
                                        builder1.setPositiveButton(
                                                "Lanjutkan",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        ((ListDokterActivity) getActivity()).openinitFragment();
                                                    }
                                                });
                                        builder1.setCancelable(true);
                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();


                                    } else {
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                        builder1.setTitle("Booking gagal");
                                        builder1.setMessage("Cek Data tidak boleh kosong");
                                        builder1.setPositiveButton(
                                                "Close",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                        builder1.setCancelable(true);
                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        });
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("jadwal_id",idjadwal);
                params.put("nama", etNama.getText().toString());
                params.put("umur", etUmur.getText().toString());
                params.put("kk", etKK.getText().toString());
                params.put("alamat", etAlamat.getText().toString());
                params.put("ansuransi", ANSURANSI);
                params.put("ref", refuser);
                params.put("status", "Booked");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }

    private void uploadPasienLogin() {
        final ProgressDialog pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.setTitle("Booking");
        pDialog.show();

        final String idakun=new SharedPref(getContext()).getIdUser();
        Toast.makeText(getContext(),idakun,Toast.LENGTH_SHORT).show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, JSON_BOOKING1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        pDialog.dismiss();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.getString("status");

                                    if (status.equalsIgnoreCase("true")) {
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                        builder1.setTitle("Booking Sukses");
                                        builder1.setMessage("Booking Telah Berhasil di Buat.");
                                        builder1.setPositiveButton(
                                                "Lanjutkan",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        ((ListDokterActivity) getActivity()).openinitFragment();
                                                    }
                                                });
                                        builder1.setCancelable(true);
                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();


                                    } else {
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                        builder1.setTitle("Booking gagal");
                                        builder1.setMessage("Cek Data tidak boleh kosong");
                                        builder1.setPositiveButton(
                                                "Close",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                        builder1.setCancelable(true);
                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        });
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Log.e("ERROR",error.toString());
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pasien_id",idakun);
                params.put("jadwal_id",idjadwal);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }




    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnBooking = (Button) view.findViewById(R.id.btnBooking);
        ckdiri = (CheckBox) view.findViewById(R.id.ckdiri);
        etNama = (EditText) view.findViewById(R.id.etNama);
        etUmur = (EditText) view.findViewById(R.id.etUmur);
        etKK = (EditText) view.findViewById(R.id.etKK);
        etAlamat = (EditText) view.findViewById(R.id.etAlamat);
        spAnsuransi = (Spinner) view.findViewById(R.id.spAnsuransi);
        etAntrian = (EditText) view.findViewById(R.id.etAntrian);

        Bundle bundle = getArguments();
        idjadwal = bundle.getString("idjadwaldetail");
        etAntrian.setText("Antrian ke " + idjadwal);
        refuser=new SharedPref(getContext()).getId();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.simple_row, R.id.tvSpinnner, data1);
        spAnsuransi.setAdapter(adapter);
        spAnsuransi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    ANSURANSI = "A";
                } else {
                    ANSURANSI = "U";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ckdiri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Mendaftar Untuk Pribadi",Toast.LENGTH_LONG).show();
                statusupload = true;
            }
        });

        btnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (statusupload) {
                    uploadPasienLogin();
                } else {
                   uploadUser();
                }
            }
        });
    }
}







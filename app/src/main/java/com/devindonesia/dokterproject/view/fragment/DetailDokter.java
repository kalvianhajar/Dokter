package com.devindonesia.dokterproject.view.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.Dokter;
import com.devindonesia.dokterproject.view.activity.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.os.Handler;


/**
 * Created by root on 22/08/17.
 */

public class DetailDokter extends Fragment{
    private RecyclerView rvTempatPraktik;
    private AdapterRecylerviewAll<JadwalDokter, DetailDokterHolder> adapter;
    private ArrayList<JadwalDokter> mData = new ArrayList<>();
    private String JSON_URL = "http://dokter.themastah.com/api/dokter";
    private String JSON_PRKATIK = "http://dokter.themastah.com/api/jadwal/dokter";
    ProgressDialog progressDialog;
    private String TAG = DetailDokter.class.getSimpleName();
    private TextView tvNamaDokter, tvSpesialis, tvNoHp;
    private String id, iduser;
    private boolean statusjadwal = true;
    private TextView tvMsg;


    @Override
    public void onDestroy() {
        super.onDestroy();
        mData.clear();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((ListDokterActivity) getActivity()).toolbar.setTitle("Detail Dokter");
        return inflater.inflate(R.layout.detaildokter, container, false);
    }

    //get data dokter
    private void sendRequest(String id) {
        StringRequest stringRequest = new StringRequest(JSON_URL + "?id=" + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                tvNamaDokter.setText("Dr. " + obj.getString("dokter_nama"));
                                tvSpesialis.setText(obj.getString("spesialis_nama"));
                                tvNoHp.setText(obj.getString("dokter_hp"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    //get jadwal berdasarkan id dokter
    private void getJadwal(String id) {

        StringRequest stringRequest = new StringRequest(JSON_PRKATIK+"?id=" + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mData.clear();

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);

                                String praktikid = obj.getString("praktik_id");
                                String praktik_nama = obj.getString("praktik_nama");
                                String praktik_alamat = obj.getString("praktik_alamat");
                                String user_id = obj.getString("dokter_id");

                                JadwalDokter jadwalDokter = new JadwalDokter(praktikid, praktik_alamat, user_id, praktik_nama);
                                mData.add(jadwalDokter);
                            }

                            if(mData.size()>0){
                                tvMsg.setVisibility(View.GONE);
                                rvTempatPraktik.setVisibility(View.VISIBLE);
                                adapter.notifyDataSetChanged();
                            }else{
                                tvMsg.setVisibility(View.VISIBLE);
                                rvTempatPraktik.setVisibility(View.GONE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvTempatPraktik = (RecyclerView) view.findViewById(R.id.rvTempatPraktik);
        tvNamaDokter = (TextView) view.findViewById(R.id.tvNamaDokter);
        tvSpesialis = (TextView) view.findViewById(R.id.tvSpesialis);
        tvMsg = (TextView) view.findViewById(R.id.tvMsg);
        tvNoHp = (TextView) view.findViewById(R.id.tvNoHp);
        //initialize the progress dialog and show it

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Dokter");
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        Bundle bundle = getArguments();
        id = bundle.getString("id");
        iduser = new SharedPref(getContext()).getId();
        adapter = new AdapterRecylerviewAll<JadwalDokter, DetailDokterHolder>(R.layout.row_detaildokter, DetailDokterHolder.class, JadwalDokter.class, mData) {
            @Override
            protected void bindView(final DetailDokterHolder holder, final JadwalDokter jadwalDokter, int position) {
                holder.setData(jadwalDokter);
                if (new SharedPref(getContext()).getLoginStatus()) {
                    holder.btnListAntrian.setVisibility(View.VISIBLE);
                    holder.btnDaftar.setVisibility(View.GONE);
                } else {
                    holder.btnListAntrian.setVisibility(View.GONE);
                    holder.btnDaftar.setVisibility(View.VISIBLE);
                }
                holder.btnListAntrian.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((ListDokterActivity) getActivity()).openJadwalPraktik(jadwalDokter.getUserid(), jadwalDokter.getId());
                    }
                });

                holder.btnJadwalPrkatik.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (statusjadwal) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    holder.container_jadwal.setVisibility(View.VISIBLE);
                                    statusjadwal = false;
                                }
                            }, 400);
                        } else {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    holder.container_jadwal.setVisibility(View.GONE);
                                    statusjadwal = true;
                                }
                            }, 400);

                        }


                    }
                });
            }
        };
        rvTempatPraktik.setLayoutManager(new LinearLayoutManager(getContext()));
        rvTempatPraktik.setAdapter(adapter);

        sendRequest(id);
        getJadwal(id);
    }




}

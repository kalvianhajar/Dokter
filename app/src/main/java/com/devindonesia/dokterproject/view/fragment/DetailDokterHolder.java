package com.devindonesia.dokterproject.view.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devindonesia.dokterproject.R;

/**
 * Created by root on 23/08/17.
 */
public class DetailDokterHolder extends RecyclerView.ViewHolder {

    private TextView tvTempatPraktik,tvAlamatPraktik;
    public Button btnJadwalPrkatik,btnListAntrian,btnDaftar;
    public LinearLayout container_jadwal;

    public DetailDokterHolder(View itemView) {
        super(itemView);
        tvTempatPraktik=(TextView)itemView.findViewById(R.id.tvTempatPraktik);
        tvAlamatPraktik=(TextView)itemView.findViewById(R.id.tvAlamatPraktik);
        btnJadwalPrkatik=(Button)itemView.findViewById(R.id.btnJadwalPrkatik);
        btnDaftar=(Button)itemView.findViewById(R.id.btnDaftar);
        btnListAntrian=(Button)itemView.findViewById(R.id.btnListAntrian);
        container_jadwal=(LinearLayout)itemView.findViewById(R.id.container_jadwal);
    }
    public void  setData(JadwalDokter jadwalDokter){
        tvTempatPraktik.setText("Jadwal Praktik "+jadwalDokter.getId());
        tvAlamatPraktik.setText(jadwalDokter.getNamapraktik()+" - "+jadwalDokter.getAlamatPraktik());

    }
}

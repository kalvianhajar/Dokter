package com.devindonesia.dokterproject.view.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.view.activity.KelolaJadwal;
import com.devindonesia.dokterproject.view.activity.KelolaPasien;
import com.devindonesia.dokterproject.view.activity.KelolasAsisten;
import com.devindonesia.dokterproject.view.activity.LaporanActivity;
import com.devindonesia.dokterproject.view.activity.ListAntrianDokter;
import com.devindonesia.dokterproject.view.activity.MainActivity;
import com.devindonesia.dokterproject.view.activity.ProfileActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 20/08/17.
 */

public class DokterFragment extends Fragment implements View.OnClickListener {
    private RelativeLayout container_profile, container_laporan, container_pasien, container_asistent, container_jadwal,container_antrian;
    private String URL = "http://dokter.themastah.com/api/auth/getdokter";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dokterfragment, container, false);
    }

    private void getDokter(final String ID) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String dokter = obj.getString("dokter_id");
                                new SharedPref(getContext()).setIdUser(dokter);
                            }

                        } catch (JSONException e) {


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", ID);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        container_profile = (RelativeLayout) view.findViewById(R.id.container_profile);
        container_laporan = (RelativeLayout) view.findViewById(R.id.container_laporan);
        container_pasien = (RelativeLayout) view.findViewById(R.id.container_pasien);
        container_asistent = (RelativeLayout) view.findViewById(R.id.container_asistent);
        container_jadwal = (RelativeLayout) view.findViewById(R.id.container_jadwal);
        container_antrian = (RelativeLayout) view.findViewById(R.id.container_antrian);
        container_profile.setOnClickListener(this);
        container_laporan.setOnClickListener(this);
        container_pasien.setOnClickListener(this);
        container_asistent.setOnClickListener(this);
        container_jadwal.setOnClickListener(this);
        container_antrian.setOnClickListener(this);
        String id = new SharedPref(getContext()).getId();
        getDokter(id);
    }

    @Override
    public void onClick(View view) {
        if (view == container_profile) {
            startActivity(new Intent(getContext(), ProfileActivity.class));
        }
        if (view == container_laporan) {
            startActivity(new Intent(getContext(), LaporanActivity.class));
        }
        if (view == container_pasien) {
            startActivity(new Intent(getContext(), KelolaPasien.class));
        }
        if (view == container_asistent) {
            startActivity(new Intent(getContext(), KelolasAsisten.class));
        }
        if (view == container_jadwal) {
            startActivity(new Intent(getContext(), KelolaJadwal.class));
        }
        if (view == container_antrian) {
            startActivity(new Intent(getContext(), ListAntrianDokter.class));
        }


    }
}

package com.devindonesia.dokterproject.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.view.activity.MainActivity;

/**
 * Created by root on 20/09/17.
 */
public class FinishBooking extends Fragment implements View.OnClickListener {
    private Button btnOk;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.finishbooking, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnOk = (Button) view.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btnOk) {
            startActivity(new Intent(getActivity(), MainActivity.class));
        }
    }
}

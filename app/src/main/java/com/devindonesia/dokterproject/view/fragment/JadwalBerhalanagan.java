package com.devindonesia.dokterproject.view.fragment;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.ListJadwal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 23/08/17.
 */
public class JadwalBerhalanagan  extends Fragment implements View.OnClickListener{
    private RecyclerView rvJadwalPraktik;
    private AdapterRecylerviewAll<ListJadwal, JadwalBerhalangan> adapter;
    private ArrayList<ListJadwal> mData = new ArrayList<>();
    private String JSON_JADWAL = "http://dokter.themastah.com/api/jadwal/getdokterjadwal";
    private EditText tvDate;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.jadwalberhalangan,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvJadwalPraktik=(RecyclerView)view.findViewById(R.id.rvJadwalPraktik);
        tvDate=(EditText)view.findViewById(R.id.tvDate);
        tvDate.setOnClickListener(this);
        rvJadwalPraktik.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new AdapterRecylerviewAll<ListJadwal, JadwalBerhalangan>(R.layout.row_berhalangan,JadwalBerhalangan.class,ListJadwal.class, mData) {
            @Override
            protected void bindView(JadwalBerhalangan holder, final ListJadwal model, final int position) {
                holder.setData(model);

                if(model.getJam().equalsIgnoreCase("ya")){
                    holder.imgStatus.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.iconoff));
                }else{
                    holder.imgStatus.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.iconon));
                }
            }
        };
        rvJadwalPraktik.setAdapter(adapter);
        String iddokter=new SharedPref(getContext()).getIdUser();
        getJadwal(iddokter);
    }

    private void getJadwal(final String dokterid) {
        mData.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST,JSON_JADWAL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);

                                String hari=obj.getString("hari");
                                String jammulai=obj.getString("jam_mulai").substring(0,5);
                                String jamakhir=obj.getString("jam_akhir").substring(0,5);
                                String aktif=obj.getString("aktif");

                                mData.add(new ListJadwal(hari,jammulai+"-"+jamakhir,aktif));
                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("iddokter", dokterid);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void showPopup(final EditText editText) {
        LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.main3, null, false);
        final PopupWindow popupWindow = new PopupWindow(layout, 400, 400);
        popupWindow.setContentView(layout);
        popupWindow.setHeight(500);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        CalendarView cv = (CalendarView) layout.findViewById(R.id.calendarView);
        cv.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        cv.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                popupWindow.dismiss();
                editText.setText(year + "-" + (month+1) + "-" + dayOfMonth);
            }
        });
        popupWindow.showAtLocation(layout, Gravity.TOP, 5, 170);
    }

    @Override
    public void onClick(View view) {
        if(view==tvDate){
            showPopup(tvDate);
        }
    }
}

package com.devindonesia.dokterproject.view.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.ListJadwal;

/**
 * Created by root on 23/10/17.
 */
public class JadwalBerhalangan extends RecyclerView.ViewHolder {

    private TextView tvHari;
    private TextView tvJam;
    public ImageView imgStatus;

    public JadwalBerhalangan(View itemView) {
        super(itemView);
        tvHari = (TextView) itemView.findViewById(R.id.tvHari);
        tvJam = (TextView) itemView.findViewById(R.id.tvJamPeriksa);
        imgStatus = (ImageView) itemView.findViewById(R.id.imgStatus);

    }

    public void setData(ListJadwal jadwal) {
        tvHari.setText(jadwal.getId());
        tvJam.setText(jadwal.getStatus());

    }
}

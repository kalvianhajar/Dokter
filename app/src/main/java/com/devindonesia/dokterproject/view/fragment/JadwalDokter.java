package com.devindonesia.dokterproject.view.fragment;

/**
 * Created by root on 23/08/17.
 */
public class JadwalDokter {
    private String id;
    private String alamatPraktik;
    private String namaPraktik;
    private String userid;
    private String namapraktik;



    public JadwalDokter(String id, String alamatPraktik, String userid, String namapraktik) {
        this.id = id;
        this.alamatPraktik = alamatPraktik;
        this.userid = userid;
        this.namapraktik = namapraktik;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getNamapraktik() {
        return namapraktik;
    }


    public void setNamapraktik(String namapraktik) {
        this.namapraktik = namapraktik;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlamatPraktik() {
        return alamatPraktik;
    }

    public void setAlamatPraktik(String alamatPraktik) {
        this.alamatPraktik = alamatPraktik;
    }

    public String getNamaPraktik() {
        return namaPraktik;
    }

    public void setNamaPraktik(String namaPraktik) {
        this.namaPraktik = namaPraktik;
    }
}



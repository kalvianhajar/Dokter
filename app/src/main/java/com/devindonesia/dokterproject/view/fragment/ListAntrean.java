package com.devindonesia.dokterproject.view.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.JadwalPraktik;
import com.devindonesia.dokterproject.model.ListJadwal;
import com.devindonesia.dokterproject.view.activity.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by root on 23/08/17.
 */
public class ListAntrean extends Fragment implements View.OnClickListener {
    private RecyclerView rvListAntrean;
    private ArrayList<ListJadwal> mData = new ArrayList<>();
    private AdapterRecylerviewAll<ListJadwal, ListAntreanHolder> adapter;
    private String JSON_URL = "http://dokter.themastah.com/api/jadwal/antrian";
    private String JSON_TGL = "http://dokter.themastah.com/api/jadwal/antriantgl";
    private String JSON_JADWAL = "http://dokter.themastah.com/api/jadwal/datajadwal";
    ProgressDialog progressDialog;
    String dokterid;
    String praktikid;
    String tgl_praktik;
    String jammulai;
    String jamselesia;
    private String id, iduser;
    Calendar myCalendar = Calendar.getInstance();
    private EditText tglTransaksi;
    private Button btnFilter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((ListDokterActivity) getActivity()).toolbar.setTitle("List Antrian");
        return inflater.inflate(R.layout.listantrean, container, false);
    }


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tglTransaksi.setText(sdf.format(myCalendar.getTime()));
    }


    //get data dokter
    private void sendRequest(String id) {
        mData.clear();
        StringRequest stringRequest = new StringRequest(JSON_URL + "?id=" + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);

                                dokterid = obj.getString("dokter_id");
                                praktikid = obj.getString("praktik_id");
                                tgl_praktik = obj.getString("tgl_praktik");
                                jammulai = obj.getString("jam_mulai").substring(0, 5);
                                jamselesia = obj.getString("jam_akhir").substring(0, 5);
                                String status = obj.getString("status");
                                String pasien = obj.getString("nama_pasien");
                                String jadwaloi = obj.getString("jadwal_id");


                                mData.add(new ListJadwal("" + (i + 1), status, jammulai + " - " + jamselesia, jadwaloi, pasien, dokterid, praktikid));
                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    //get data dokter

    private void getJadwal(final String dokterid, final String tgl, final String praktik) {
        mData.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, JSON_JADWAL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject obj = jsonArray.getJSONObject(i);
                                jammulai = obj.getString("jam_praktik").substring(0, 5);

                                String status = obj.getString("status");
                                String pasien = obj.getString("pasien_nama");
                                String jadwalid = obj.getString("jadwal_detail_id");

                                mData.add(new ListJadwal("" + (i + 1), status, jammulai, jadwalid, pasien));

                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dokter_id", dokterid);
                params.put("tgl", tgl);
                params.put("praktik_id", praktik);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvListAntrean = (RecyclerView) view.findViewById(R.id.rvListAntrean);
        tglTransaksi = (EditText) view.findViewById(R.id.tglTransaksi);
        btnFilter = (Button) view.findViewById(R.id.btnFilter);
        rvListAntrean.setLayoutManager(new LinearLayoutManager(getContext()));
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Jadwal");
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        mData.clear();
        adapter = new AdapterRecylerviewAll<ListJadwal, ListAntreanHolder>(R.layout.row_listjadwal,
                ListAntreanHolder.class, ListJadwal.class, mData) {
            @Override
            protected void bindView(ListAntreanHolder holder, final ListJadwal model, final int position) {
                holder.setData(model);
                holder.container_booking.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String status=model.getStatus();
                        if(status.equalsIgnoreCase("Booked") || status.equalsIgnoreCase("Done")) {
                            Toast.makeText(getContext(), "No Antrian ini Telah Di Booking", Toast.LENGTH_SHORT).show();
                        }else {
                            ((ListDokterActivity) getActivity()).openDetailBooking(model.getIdpasien());
                        }
                    }

                });
            }
        };

        rvListAntrean.setAdapter(adapter);
        Bundle bundle = getArguments();
        final String id = bundle.getString("id");
        praktikid = bundle.getString("praktikid");

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        String day1, month1;


        if (String.valueOf(day).length() < 2) {
            day1 = "0" + day;
        } else {
            day1 = String.valueOf(day);
        }
        if (String.valueOf(month).length() < 2) {
            month1 = "0" + month;
        } else {
            month1 = String.valueOf(month);
        }


        String tgl = year + "-" + month1 + "-" + day1;
        getJadwal(id, tgl, praktikid);
        tglTransaksi.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view == tglTransaksi) {
            // TODO Auto-generated method stub
            new DatePickerDialog(getContext(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }

        if (view == btnFilter) {
            String tglfilter = tglTransaksi.getText().toString();

            if (!TextUtils.isEmpty(tglfilter)) {
                getJadwal(id, tglfilter, praktikid);

            } else {
                Toast.makeText(getContext(), "Silahkan Pilih Tanggal/Jadwal", Toast.LENGTH_LONG).show();
            }
        }
    }


}

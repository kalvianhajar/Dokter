package com.devindonesia.dokterproject.view.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.ListJadwal;

/**
 * Created by root on 23/08/17.
 */
public class ListAntreanHolder extends RecyclerView.ViewHolder
{
    public TextView tvNoBooking,tvStatus,tvJamPeriksa,tvNamaPasien,tvDone;
    public LinearLayout container_booking;

    public ListAntreanHolder(View itemView) {
        super(itemView);
        tvNoBooking=(TextView)itemView.findViewById(R.id.tvNoBooking);
        tvStatus=(TextView)itemView.findViewById(R.id.tvStatus);
        tvJamPeriksa=(TextView)itemView.findViewById(R.id.tvJamPeriksa);
        tvNamaPasien=(TextView)itemView.findViewById(R.id.tvNamaPasien);
        tvDone=(TextView)itemView.findViewById(R.id.tvDone);
        container_booking=(LinearLayout)itemView.findViewById(R.id.container_booking);
    }

    public void setData(ListJadwal listJadwal){
        tvNoBooking.setText(listJadwal.getId());
        tvStatus.setText(listJadwal.getStatus());
        tvJamPeriksa.setText(listJadwal.getJam());
        tvNamaPasien.setText(listJadwal.getNamapasien());
    }

}

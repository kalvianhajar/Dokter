package com.devindonesia.dokterproject.view.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.Dokter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by root on 22/08/17.
 */

public class ListDokter extends Fragment {
    private RecyclerView rvDokter;
    private AdapterRecylerviewAll<Dokter,ListDokterHolder> adapter;
    private ArrayList<Dokter> dokterArrayList=new ArrayList<>();
    private String JSON_URL="http://dokter.themastah.com/api/dokter";
    private String TAG=ListDokter.class.getSimpleName();
    ProgressDialog progressDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.listdokter,container,false);
    }
    private void sendRequest(){
        dokterArrayList.clear();
        StringRequest stringRequest = new StringRequest(JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response.toString());
                            JSONArray jsonArray=new JSONArray(jsonObject.getString("data"));
                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject obj=jsonArray.getJSONObject(i);
                                String nama=obj.getString("dokter_nama");
                                String id=obj.getString("dokter_id");
                                String spesialis=obj.getString("spesialis_nama");
                                Dokter dokter=new Dokter(id,nama,spesialis);
                                dokterArrayList.add(dokter);
                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("ERROR",error.toString());
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menudokter, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter:
                Toast.makeText(getContext(),"FIlter Dokter",Toast.LENGTH_LONG).show();
                return false;
            default:
                break;
        }

        return false;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        rvDokter = (RecyclerView)view.findViewById(R.id.rvDokter);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Dokter");
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        rvDokter.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter =new AdapterRecylerviewAll<Dokter, ListDokterHolder>(R.layout.row_dokter,ListDokterHolder.class,Dokter.class,dokterArrayList) {
            @Override
            protected void bindView(ListDokterHolder holder, final Dokter dokter, int position) {
                holder.initData(dokter);
                holder.container_dokter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((ListDokterActivity)getActivity()).openListDokter(dokter.getId());
                    }
                });
            }
        };
        rvDokter.setAdapter(adapter);
        sendRequest();

    }


}

package com.devindonesia.dokterproject.view.fragment;

import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.android.volley.toolbox.StringRequest;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.R;

import java.util.ArrayList;

public class ListDokterActivity extends AppCompatActivity {
    public Toolbar toolbar;
    private String TAG=ListDokterActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_dokter);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Data Dokter");
        initFragment();
    }


    @Override
    public void onBackPressed(){
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }

    private void initFragment(){
        getSupportFragmentManager().beginTransaction().add(R.id.container_listdokter,new ListDokter(),TAG).addToBackStack("init").commit();

    }

    public void openinitFragment(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container_listdokter,new PasienFragment(),TAG).addToBackStack("pasien").commit();

    }
    public void openListDokter(String id){
        Bundle bundle=new Bundle();
        bundle.putString("id",id);
        DetailDokter detailDokter=new DetailDokter();
        detailDokter.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.container_listdokter,detailDokter,TAG).addToBackStack("detail").commit();

    }

    public void openDetailBooking(String id){
        Bundle bundle=new Bundle();
        bundle.putString("idjadwaldetail",id);
        DetailBooking booking=new DetailBooking();
        booking.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.container_listdokter,booking,TAG).addToBackStack("detail").commit();
    }

    public void openJadwalPraktik(String id,String praktik) {
        Bundle bundle=new Bundle();
        bundle.putString("id",id);
        bundle.putString("praktikid",praktik);
        ListAntrean antrean=new ListAntrean();
        antrean.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.container_listdokter,antrean,TAG).addToBackStack("antrian").commit();
    }


    public void komplit() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container_listdokter,new FinishBooking(),TAG).addToBackStack("antrian").commit();
    }
    public void addPraktik() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container_listdokter,new AddPraktik(),TAG).addToBackStack("addpraktik").commit();
    }

}

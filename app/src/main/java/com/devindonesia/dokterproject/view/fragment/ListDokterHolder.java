package com.devindonesia.dokterproject.view.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.Dokter;
import com.devindonesia.dokterproject.view.activity.Booking;

/**
 * Created by root on 22/08/17.
 */
public class ListDokterHolder  extends RecyclerView.ViewHolder{
    private TextView tvNamaDokter,tvSpesialis,tvPraktik,tvStatus;
    public LinearLayout container_dokter;

    public ListDokterHolder(View v) {
        super(v);
        tvNamaDokter=(TextView)v.findViewById(R.id.tvNamaDokter);
        tvPraktik=(TextView)v.findViewById(R.id.tvPraktik);
        tvSpesialis=(TextView)v.findViewById(R.id.tvSpesialis);
        tvStatus=(TextView)v.findViewById(R.id.tvStatus);
        container_dokter=(LinearLayout) v.findViewById(R.id.container_dokter);
    }

    public void initData(Dokter dokter){
        tvNamaDokter.setText(dokter.getNama());
        tvSpesialis.setText(dokter.getSpesiali());
    }

    public void setBooking(Booking b){
        tvNamaDokter.setText("Tgl Booking : "+b.getNobooking());
        tvSpesialis.setText(b.getWaktu());
        tvPraktik.setText(b.getAlamat());
        tvStatus.setText(b.getStatus());

    }
}

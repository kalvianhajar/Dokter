package com.devindonesia.dokterproject.view.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Adapter.AdapterRecylerviewAll;
import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.ListJadwal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 23/10/17.
 */

public class ListJadwalDokter extends Fragment {
    private RecyclerView rvListPraktik;
    private ArrayList<ListJadwal> mData = new ArrayList<>();
    private AdapterRecylerviewAll<ListJadwal, ListPraktik> adapter;
    private String JSON_JADWAL = "http://dokter.themastah.com/api/jadwal/getpraktik";
    private ProgressDialog pDialog;
    private String iddokter, jammulai, jamselesai, praktikid;
    private String URL_PRAKTIK = "http://dokter.themastah.com/api/praktik";

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menujadwal, menu);
        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addJadwal:
                popUp();
                break;
            default:
                break;
        }

        return false;
    }



    private void popUp() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.addpraktik);

        TextView cancel = (TextView) dialog.findViewById(R.id.button_cancel);
        Button save = (Button) dialog.findViewById(R.id.btnSimpan);

        final EditText etNama = (EditText) dialog.findViewById(R.id.etNama);
        final EditText etDeskripsi = (EditText) dialog.findViewById(R.id.etDeskripsi);
        final EditText etAlamat = (EditText) dialog.findViewById(R.id.etAlamat);

        dialog.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(getContext(), "Cancel", Toast.LENGTH_LONG).show();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sendData(etNama.getText().toString(), etDeskripsi.getText().toString(), etAlamat.getText().toString());

            }
        });

    }


    private void sendData(final String nama, final String deksripsi, final String alamat) {
        pDialog.setTitle("Buat Tempat Praktik");
        pDialog.setMessage("Mohon tunggu...");
        pDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_PRAKTIK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            if(status){
                                Toast.makeText(getContext(),"Tempat Praktik Telah Di Buat",Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getContext(),"jadwal Praktik Lebih Dari 3 Tempat",Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dokter_id", iddokter);
                params.put("praktik_nama", nama);
                params.put("praktik_ket", deksripsi);
                params.put("praktik_alamat", alamat);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.listjadwaldokter,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvListPraktik=(RecyclerView)view.findViewById(R.id.rvListPraktik);
        rvListPraktik.setLayoutManager(new LinearLayoutManager(getContext()));
        mData.clear();
        adapter = new AdapterRecylerviewAll<ListJadwal,ListPraktik>(R.layout.row_praktik,
                ListPraktik.class, ListJadwal.class, mData) {
            @Override
            protected void bindView(ListPraktik holder, final ListJadwal model, final int position) {
                holder.setData(model);
                holder.container_praktik.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentManager manager = getFragmentManager();
                        Fragment frag = manager.findFragmentByTag("fragment_edit_name");

                        if (frag != null) {
                            manager.beginTransaction().remove(frag).commit();
                        }
                        ListKelolaJadwal editNameDialog = new ListKelolaJadwal();
                        editNameDialog.show(manager, "fragment_edit_name");
                    }
                });
            }
        };
        rvListPraktik.setAdapter(adapter);
        String id=new SharedPref(getContext()).getIdUser();
        getJadwal(id);

    }

    private void getJadwal(final String dokterid) {
        mData.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, JSON_JADWAL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object=jsonArray.getJSONObject(i);
                                String praktiknama=object.getString("praktik_nama");
                                String praktik=object.getString("praktik_alamat");
                                mData.add(new ListJadwal(""+(i+1),praktiknama,praktik));
                            }

                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dokter_id", dokterid);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }



}

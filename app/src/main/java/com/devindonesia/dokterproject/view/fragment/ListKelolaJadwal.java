package com.devindonesia.dokterproject.view.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.Helper.TimeDialog;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.ListJadwal;
import com.devindonesia.dokterproject.model.Praktik;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 23/08/17.
 */
public class ListKelolaJadwal extends DialogFragment implements View.OnClickListener {
    private Button btnSimpan,btnClose;
    private EditText etMulai, etAkhir;
    private Spinner spPraktik;
    private String URL = "http://dokter.themastah.com/api/praktik";
    private String URL_JADWAL = "http://dokter.themastah.com/api/jadwal";
    private String URL_PRAKTIK = "http://dokter.themastah.com/api/praktik";
    private ArrayList<Praktik> mData = new ArrayList<>();
    private String id, nama, ket;
    private ProgressDialog pDialog;
    private String iddokter, jammulai, jamselesai, praktikid;
    private ArrayList<String> namaRUmahsakit = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private ArrayList<String> hari=new ArrayList<>();
    private Spinner spHari;
    private String hariPilihan;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.listkelolajadwal, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView(view);
        setHasOptionsMenu(true);
        hari.add("Senin");
        hari.add("Selasa");
        hari.add("Rabu");
        hari.add("Kamis");
        hari.add("Jumat");
        hari.add("Sabtu");
        hari.add("Minggu");
        spHari=(Spinner)view.findViewById(R.id.spHari);
        spHari.setAdapter(new ArrayAdapter<String>(getContext(), R.layout.simple_row, R.id.tvSpinnner,hari));
        spHari.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                hariPilihan=hari.get(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        initializeUI();
    }




    private void popUp() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.addpraktik);

        TextView cancel = (TextView) dialog.findViewById(R.id.button_cancel);
        Button save = (Button) dialog.findViewById(R.id.btnSimpan);

        final EditText etNama = (EditText) dialog.findViewById(R.id.etNama);
        final EditText etDeskripsi = (EditText) dialog.findViewById(R.id.etDeskripsi);
        final EditText etAlamat = (EditText) dialog.findViewById(R.id.etAlamat);

        dialog.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(getContext(), "Cancel", Toast.LENGTH_LONG).show();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sendData(etNama.getText().toString(), etDeskripsi.getText().toString(), etAlamat.getText().toString());

            }
        });

    }

    private void sendData(final String nama, final String deksripsi, final String alamat) {
        pDialog.setTitle("Buat Tempat Praktik");
        pDialog.setMessage("Mohon tunggu...");
        pDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_PRAKTIK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            if(status){
                                Toast.makeText(getContext(),"Tempat Praktik Telah Di Buat",Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getContext(),"jadwal Praktik Lebih Dari 3 Tempat",Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dokter_id", iddokter);
                params.put("praktik_nama", nama);
                params.put("praktik_ket", deksripsi);
                params.put("praktik_alamat", alamat);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);

    }


    private void initializeUI() {
        iddokter = new SharedPref(getContext()).getIdUser();
        pDialog = new ProgressDialog(getContext());

        pDialog.setIndeterminate(true);
        pDialog.setTitle("Create Jadwal");
        pDialog.setMessage("Mohon menunggu...");

        adapter = new ArrayAdapter<String>(getContext(), R.layout.simple_row, R.id.tvSpinnner, namaRUmahsakit);
        spPraktik.setAdapter(adapter);
        getPraktik();
        spPraktik.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                praktikid = mData.get(i).getId().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Silahkan pilih tempat praktik", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        etMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeDialog dialog = TimeDialog.newInstance(view);
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                dialog.show(ft, "TimeDialog");
            }
        });

        etAkhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeDialog dialog = TimeDialog.newInstance(view);
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                dialog.show(ft, "TimeDialog");
            }
        });

    }

    private void postJadwal(final String jam1, final String jam2) {
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_JADWAL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        etMulai.setText("");
                                        etAkhir.setText("");
                                        Toast.makeText(getContext(), "Jadwal berhasil di buat", Toast.LENGTH_LONG).show();
                                    }
                                });

                            } else {
                                Toast.makeText(getContext(), "Kesalahan input waktu praktik", Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dokter_id", iddokter);
                params.put("jam_mulai", jam1);
                params.put("jam_akhir", jam2);
                params.put("praktik_id", praktikid);
                params.put("hari",hariPilihan);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private void getPraktik() {
        StringRequest stringRequest = new StringRequest(URL + "?id=" + iddokter,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                id = obj.getString("praktik_id");
                                nama = obj.getString("praktik_nama");
                                ket = obj.getString("praktik_alamat");

                                namaRUmahsakit.add(nama);
                                mData.add(new Praktik(id, nama, ket));
                            }
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void setupView(View view) {
        btnSimpan = (Button) view.findViewById(R.id.btnSimpan);
        btnClose = (Button) view.findViewById(R.id.btnClose);
        etMulai = (EditText) view.findViewById(R.id.tvMulai);
        etAkhir = (EditText) view.findViewById(R.id.tvAkhir);
        spPraktik = (Spinner) view.findViewById(R.id.spPraktik);
        btnSimpan.setOnClickListener(this);
        btnClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btnSimpan) {
            jammulai = etMulai.getText().toString();
            jamselesai = etAkhir.getText().toString();

            if (!TextUtils.isEmpty(jammulai) && !TextUtils.isEmpty(jamselesai)) {
                postJadwal(jammulai, jamselesai);
            } else {
                Toast.makeText(getContext(), "Silahkan isi jadwal terlebih dahulu", Toast.LENGTH_SHORT).show();
            }

        }
        if(view==btnClose){
            dismiss();
            Toast.makeText(getContext(),"Close Dialog",Toast.LENGTH_SHORT).show();
        }
    }
}


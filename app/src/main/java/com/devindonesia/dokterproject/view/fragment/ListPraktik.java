package com.devindonesia.dokterproject.view.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.model.ListJadwal;

/**
 * Created by root on 23/10/17.
 */
public class ListPraktik extends RecyclerView.ViewHolder {

    private TextView tvTempatPraktik, tvAlamatPraktik;
    public LinearLayout container_praktik;

    public ListPraktik(View itemView) {
        super(itemView);
        tvTempatPraktik = (TextView) itemView.findViewById(R.id.tvTempatPraktik);
        tvAlamatPraktik = (TextView) itemView.findViewById(R.id.tvAlamatPraktik);
        container_praktik = (LinearLayout) itemView.findViewById(R.id.container_praktik);
    }

    public void setData(ListJadwal jadwal) {
        tvTempatPraktik.setText(jadwal.getStatus());
        tvAlamatPraktik.setText(jadwal.getJam());
    }
}

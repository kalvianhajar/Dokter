package com.devindonesia.dokterproject.view.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.view.activity.MainActivity;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.view.activity.UserActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 20/08/17.
 */

public class LoginFragment extends Fragment implements View.OnClickListener{
    private Button btnProsesLogin;
    private TextView tvRegister;
    private EditText tvUsername,tvPassword;
    private String JSON_URL2 = "http://dokter.themastah.com/api/auth";
    private String TAG=LoginFragment.class.getSimpleName();
    private SharedPref pref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.loginfragment,container,false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(pref.getLoginStatus()){
            startActivity(new Intent(getContext(),MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref=new SharedPref(getContext());
        btnProsesLogin=(Button)view.findViewById(R.id.btnProsesLogin);
        tvRegister=(TextView) view.findViewById(R.id.tvRegister);
        tvPassword=(EditText)view.findViewById(R.id.tvPassword);
        tvUsername=(EditText)view.findViewById(R.id.tvUsername);
        btnProsesLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view==btnProsesLogin){
            final ProgressDialog pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Loading...");
            pDialog.setTitle("Login");
            pDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, JSON_URL2,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(final String response) {
                            pDialog.dismiss();
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {

                                        JSONObject jsonObject=new JSONObject(response);
                                        String status=jsonObject.getString("status");

                                        if(status.equalsIgnoreCase("true")){
                                            JSONArray jsonArray=jsonObject.getJSONArray("data");

                                            int lengthobt=jsonArray.length();

                                            for(int i=0;i<lengthobt;i++){
                                                JSONObject obj=jsonArray.getJSONObject(i);
                                                pref.setUsername(obj.getString("username"));
                                                pref.setLevel(obj.getString("level"));
                                                pref.setId(obj.getString("id"));
                                            }

                                            //set user login
                                            pref.setLogin(true);

                                            startActivity(new Intent(getContext(),MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

                                            Toast.makeText(getActivity(),"Login Sukses",Toast.LENGTH_LONG).show();

                                        }else{
                                            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                            builder1.setTitle("Login gagal");
                                            builder1.setMessage("Username dan password tidak terdaftar..");
                                            builder1.setPositiveButton(
                                                    "Close",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });
                                            builder1.setCancelable(true);
                                            AlertDialog alert11 = builder1.create();
                                            alert11.show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            });
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pDialog.dismiss();
                            Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {


                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", tvUsername.getText().toString());
                    params.put("password", tvPassword.getText().toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        }
        if(view==tvRegister){
            ((UserActivity)getActivity()).addRegister();
        }
    }


}

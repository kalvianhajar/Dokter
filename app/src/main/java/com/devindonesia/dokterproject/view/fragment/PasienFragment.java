package com.devindonesia.dokterproject.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devindonesia.dokterproject.Helper.SharedPref;
import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.view.activity.ListAntrianRealtime;
import com.devindonesia.dokterproject.view.activity.ListHistory;
import com.devindonesia.dokterproject.view.activity.ListRumahSakit;
import com.devindonesia.dokterproject.view.activity.ProfileActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 20/08/17.
 */

public class PasienFragment extends Fragment {
    private RelativeLayout container_dokter, container_profile, container_listantrian, container_history;
    private String URL = "http://dokter.themastah.com/api/auth/getpasien";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pasienfragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        container_dokter = (RelativeLayout) view.findViewById(R.id.container_dokter);
        container_profile = (RelativeLayout) view.findViewById(R.id.container_profile);
        container_history = (RelativeLayout) view.findViewById(R.id.container_history);
        String id = new SharedPref(getContext()).getId();

        getPasien(id);
        container_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ListHistory.class));
            }
        });
        container_listantrian = (RelativeLayout) view.findViewById(R.id.container_listantrian);
        container_listantrian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ListRumahSakit.class));
            }
        });
        container_dokter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ListDokterActivity.class));
            }
        });
        container_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ProfileActivity.class));
            }
        });


    }

    private void getPasien(final String ID) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String pasien = obj.getString("pasien_id");
                                new SharedPref(getContext()).setIdUser(pasien);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", ID);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }
}


package com.devindonesia.dokterproject.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.devindonesia.dokterproject.R;
import com.devindonesia.dokterproject.view.activity.FormRegisterDokterActivity;
import com.devindonesia.dokterproject.view.activity.FormRegisterPasienActivity;
import com.devindonesia.dokterproject.view.activity.UserActivity;

/**
 * Created by root on 20/08/17.
 */

public class RegisterFragment extends Fragment implements View.OnClickListener{
    private TextView btnLogin;
    private String[] data={"Dokter","Pasien"};
    private Spinner spUser;
    private int idKategori=0;
    private Button btnRegisterPasien,btnRegisterDokter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.registerfragment,container,false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spUser=(Spinner)view.findViewById(R.id.spUser);
        btnRegisterDokter=(Button)view.findViewById(R.id.btnRegisterDokter);
        btnRegisterDokter.setOnClickListener(this);
        btnRegisterPasien=(Button)view.findViewById(R.id.btnRegisterPasien);
        btnRegisterPasien.setOnClickListener(this);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,data);
        btnLogin=(TextView) view.findViewById(R.id.btnProses);
        btnLogin.setOnClickListener(this);
        spUser.setAdapter(adapter);
        spUser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                idKategori=spUser.getSelectedItemPosition()+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view==btnLogin){
            ((UserActivity)getActivity()).replaceLogin();
        }
        if(view==btnRegisterDokter){
            Intent intent=new Intent(getContext(),FormRegisterDokterActivity.class);
            startActivity(intent);
        }
        if(view==btnRegisterPasien){
            Intent intent=new Intent(getContext(),FormRegisterPasienActivity.class);
            startActivity(intent);
        }
    }
}
